/*
 * $Id: ChannelRefresh.java 134 2008-12-24 21:11:01Z michael.novakjr $
 *
 * Copyright (C) 2007 Josh Guilfoyle <jasta@devtcg.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * TODO: This class needs to be generalized much better, with specialized
 * parsers for Atom 1.0, Atom 0.3, RSS 0.91, RSS 1.0 and RSS 2.0.  Hell,
 * this whole thing needs to be chucked and redone.
 */

package net.marvinlee.parser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import net.marvinlee.thestar.RowData;
import net.marvinlee.util.DateUtils;

import org.apache.commons.lang.StringEscapeUtils;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

/** TODO: this class needs lots of work, content of some feeds is getting all messed up, no good. **/
public class ChannelRefresh extends DefaultHandler
{
	private static final String TAG = "RSSChannelRefresh";

	private Handler mHandler;
	private long mID;
	private String mRSSURL;
	private Vector<RowData> mData;

	private ContentResolver mContent;

	/* Buffer post information as we learn it in STATE_IN_ITEM. */
	private ChannelPost mPostBuf;

	/* Efficiency is the name of the game here... */
	private int mState;
	private static final int STATE_IN_ITEM = (1 << 2);
	private static final int STATE_IN_ITEM_TITLE = (1 << 3);
	private static final int STATE_IN_ITEM_LINK = (1 << 4);
	private static final int STATE_IN_ITEM_DESC = (1 << 5);
	private static final int STATE_IN_ITEM_DATE = (1 << 6);
	private static final int STATE_IN_ITEM_AUTHOR = (1 << 7);
	private static final int STATE_IN_TITLE = (1 << 8);

	private static HashMap<String, Integer> mStateMap;

	private String currentTitle = "";
	private boolean titleEnded = true;
	
	private String currentLink = "";
	private boolean linkEnded = true;
	
	
	static
	{
		mStateMap = new HashMap<String, Integer>();
		mStateMap.put("item", new Integer(STATE_IN_ITEM));
		mStateMap.put("entry", new Integer(STATE_IN_ITEM));
		mStateMap.put("title", new Integer(STATE_IN_ITEM_TITLE));
		mStateMap.put("link", new Integer(STATE_IN_ITEM_LINK));
		mStateMap.put("description", new Integer(STATE_IN_ITEM_DESC));
		mStateMap.put("content", new Integer(STATE_IN_ITEM_DESC));
		mStateMap.put("content:encoded", new Integer(STATE_IN_ITEM_DESC));
		mStateMap.put("dc:date", new Integer(STATE_IN_ITEM_DATE));
		mStateMap.put("updated", new Integer(STATE_IN_ITEM_DATE));
		mStateMap.put("pubDate", new Integer(STATE_IN_ITEM_DATE));
		mStateMap.put("dc:author", new Integer(STATE_IN_ITEM_AUTHOR));
		mStateMap.put("author", new Integer(STATE_IN_ITEM_AUTHOR));
	}

	public ChannelRefresh(ContentResolver resolver)
	{
		super();
		mContent = resolver;
	}
	
	public boolean getFeed(Handler h,  String rssurl, Vector<RowData> data, Context mContext, ProgressBar progressBar, TextView progressText) 
	{
		final int MAX_BUFFER_SIZE = 1024;
	try {
		mHandler = h;
		mRSSURL = rssurl;
		mData = data;

		Log.i("TheStar","Preparing to download");
		SAXParserFactory spf = SAXParserFactory.newInstance();
		SAXParser sp = spf.newSAXParser();
		XMLReader xr = sp.getXMLReader();

		xr.setContentHandler(this);
		xr.setErrorHandler(this);

		boolean downloading = true;
		int downloaded = 0;
		int size = -1;
		URL url = new URL(mRSSURL); 
		DecimalFormat myFormatter = new DecimalFormat("###");
		
		HttpURLConnection c = (HttpURLConnection) url.openConnection();
		c.setRequestProperty("User-Agent", "Android/1.0");
		c.connect();
		int responseCode = c.getResponseCode();

		Log.i("TheStar","Response code from server=" + responseCode);
		if (responseCode != 200) {
			return false;
		}
		int contentLength = c.getContentLength();
		Log.i("TheStar","contentLength from server=" + contentLength);

		if (contentLength < 1) {
			return false;

		}
		if (size == -1) {

			size = contentLength;

		}
		//Log.i("TheStar","Getting external Storage dir");
		//File root = Environment.getExternalStorageDirectory(); 
		//Log.i("TheStar","Root=" + root.getPath());
		//if (root.canWrite()) {

			//Log.i("TheStar","Root can write");
			//File xmlfile = new File(root, "thestar.xml");
			// FileWriter xmlwriter = new FileWriter(xmlfile);
			//FileOutputStream out = new FileOutputStream(xmlfile);
			FileOutputStream out = mContext.openFileOutput("thestar.xml", 0);
			// BufferedWriter out = new BufferedWriter(xmlwriter);

			InputStream stream = null;
			stream = c.getInputStream();
			while (downloading) {
				/*
				 * Size buffer according to how much of the file is left to
				 * download.
				 */
				byte buffer[];
				if (size - downloaded > MAX_BUFFER_SIZE) {
					buffer = new byte[MAX_BUFFER_SIZE];
				} else {
					buffer = new byte[size - downloaded];
				}

				// Read from server into buffer.
				int read = stream.read(buffer);
				if (read == -1)
					break;

				// Write buffer to file.
				out.write(buffer, 0, read);
				downloaded += read;
				float progress = (float)( (downloaded* 100) / size) ;
				//Log.i("TheStar", "progress:" + downloaded* 100 + "/" + size + " " +progress);
				int progressInt = Integer.parseInt(myFormatter.format(progress)); 
				progressBar.setProgress(progressInt);
				/* Can't update with this thread
				if (progressInt == 100)
				{
					progressText.setText("Processing data...");					
				}
				*/
			}

			// Do the parsing
			FileInputStream fileInputStream = mContext.openFileInput("thestar.xml");
			xr.parse(new InputSource(fileInputStream));
			return true;
/*
		} else {
			Log.i("TheStar","Root cannot write");
			return false;
		}
		*/
	} catch (Exception e) {
		Log.i("TheStar","Error found!");
		Log.i("TheStar", e.getMessage());
		e.printStackTrace();
		return false;
	}
	}
	/*
	 * Note that if syncDB is called with id == -1, this class will interpret
	 * that to mean that a new channel is being added (and also tested) so
	 * the first meaningful piece of data encountered will trigger an insert
	 * into the database.
	 *
	 * This logic is all just terrible, but this entire class needs to be
	 * scrapped and redone to make room for improved cooperation with the rest
	 * of the application.
	 */
	public long syncDB(Handler h, long id, String rssurl)
	  throws Exception
	{
		mHandler = h;
		mID = id;
		mRSSURL = rssurl;

		SAXParserFactory spf = SAXParserFactory.newInstance();
		SAXParser sp = spf.newSAXParser();
		XMLReader xr = sp.getXMLReader();

		xr.setContentHandler(this);
		xr.setErrorHandler(this);

		URL url = new URL(mRSSURL);
		
		URLConnection c = url.openConnection();
		c.setRequestProperty("User-Agent", "Android/1.0");
		xr.parse(new InputSource(c.getInputStream()));

		return mID;
	}

	 
 
	public void startElement(String uri, String name, String qName,
			Attributes attrs)
	{
		//Log.d("XMLParse", "the tag name is: " + name);
	
		/* HACK: when we see <title> outside of an <item>, assume it's the
		 * feed title.  Only do this when we are inserting a new feed. */
		if (mID == -1 &&
		    name.equals("title") && (mState & STATE_IN_ITEM) == 0)
		{
			mState |= STATE_IN_TITLE;
			return;
		}

		Integer state = mStateMap.get(name);

		if (state != null)
		{
			mState |= state.intValue();

			if (state.intValue() == STATE_IN_ITEM)
				mPostBuf = new ChannelPost();
			else if ((mState & STATE_IN_ITEM) != 0 && state.intValue() == STATE_IN_ITEM_LINK)
			{
				String href = attrs.getValue("href");

				if (href != null)
					mPostBuf.link = href;
			}
		}
	}

	public void endElement(String uri, String name, String qName)
	{
		Integer state = mStateMap.get(name);

		if (state != null)
		{
			mState &= ~(state.intValue());

			if (state.intValue() == STATE_IN_ITEM)
			{
				if (mID == -1)
				{
					Log.d(TAG, "Oops, </item> found before feed title and our parser sucks too much to deal.");
					return;
				}

				/*
				String[] dupProj =
				  new String[] { RSSReader.Posts._ID };

				Uri listURI =
				  ContentUris.withAppendedId(RSSReader.Posts.CONTENT_URI_LIST, mID);

				Cursor dup = mContent.query(listURI,
					dupProj, "title = ? AND url = ?",
					new String[] { mPostBuf.title, mPostBuf.link}, null);
*/
				Log.d(TAG, "Post: " + mPostBuf.title);

				//Log.d("TheStar", mPostBuf.desc);
				//Log.d("TheStar", "Putting to row data" + mPostBuf.getDate());

				//if (dup.getCount() == 0)
				//{
	                RowData rd = new RowData(mPostBuf.title,mPostBuf.link, mPostBuf.desc, mPostBuf.getDate());
	        		mData.add(rd); 
	        		titleEnded = true;
	        		linkEnded = true;
					
					/*
					ContentValues values = new ContentValues();

					values.put(RSSReader.Posts.CHANNEL_ID, mID);
					values.put(RSSReader.Posts.TITLE, mPostBuf.title);
					values.put(RSSReader.Posts.URL, mPostBuf.link);
					values.put(RSSReader.Posts.AUTHOR, mPostBuf.author);
					values.put(RSSReader.Posts.DATE, mPostBuf.getDate());
					values.put(RSSReader.Posts.BODY, mPostBuf.desc);

					Uri added = mContent.insert(RSSReader.Posts.CONTENT_URI, values);
					*/
					//Log.d("ChannelRefresh", "Inserting new post: " + rd.toString());
				//}

				//dup.close();
			}
		}
	}

	public void characters(char ch[], int start, int length)
	{
		/* HACK: This is the other side of the above hack in startElement. */
		if (mID == -1 && (mState & STATE_IN_TITLE) != 0)
		{
			ContentValues values = new ContentValues();
/*
			values.put(RSSReader.Channels.TITLE, new String(ch, start, length));
			values.put(RSSReader.Channels.URL, mRSSURL);

			Uri added =
			  mContent.insert(RSSReader.Channels.CONTENT_URI, values);

			Log.d("Channel Insert", "Uri added is: " + added.toString());
			mID = Long.parseLong(added.getPathSegments().get(1));

				*/
			/* There's no reason we need to do this ever, but we'll just be
			 * good about removing this awful hack from runtime data. */
			mState &= ~STATE_IN_TITLE;

			return;
		}

		if ((mState & STATE_IN_ITEM) == 0)
			return;

		/*
		 * We sort of pretended that mState was inclusive, but really only
		 * STATE_IN_ITEM is inclusive here.  This is a goofy design, but it is
		 * done to make this code a bit simpler and more efficient.
		 */
		switch (mState)
		{
		case STATE_IN_ITEM | STATE_IN_ITEM_TITLE:
			if (titleEnded)
			{
				currentTitle = "";
				titleEnded = false;
			} 
			
			currentTitle+= StringEscapeUtils.unescapeHtml(new String(ch, start, length));
			//currentTitle+= stringToHTMLString(new String(ch, start, length));
			mPostBuf.title = currentTitle;
			Log.d("ChannelRefresh","Title found is :" + mPostBuf.title);  
			break;
		case STATE_IN_ITEM | STATE_IN_ITEM_DESC:
			mPostBuf.desc += (new String(ch, start, length));
			//mPostBuf.desc += stringToHTMLString(new String(ch, start, length));
			//String hr = "<hr noshade size=&quot;1&quot; width=&quot;50%&quot;>";
			String hr = "<hr noshade size=\"1\" width=\"50%\">";
			if (mPostBuf.desc.indexOf(hr) > 0)
			{
				mPostBuf.desc = mPostBuf.desc.substring(0, mPostBuf.desc.indexOf(hr));
			}
			break;
		case STATE_IN_ITEM | STATE_IN_ITEM_LINK:
			if (linkEnded)
			{
				currentLink = "";
				linkEnded = false;
			} 
			currentLink += stringToHTMLString(new String(ch, start, length));
			mPostBuf.link = currentLink;
			//Log.d("ChannelRefresh","Link found is :" + mPostBuf.link);  
			break;
		case STATE_IN_ITEM | STATE_IN_ITEM_DATE:
			mPostBuf.setDate(new String(ch, start, length));
			//Log.d("ChannelRefresh","Date found is :" + mPostBuf.getDate()); 
			break;
		case STATE_IN_ITEM | STATE_IN_ITEM_AUTHOR:
			mPostBuf.author = new String(ch, start, length);
			break;
		default:
			/* Don't care... */
		}
	}

	public static String stringToHTMLString(String string) {
		
		// First do reverse escape
		//string = unescapeHTML(string, 0);
		
	    StringBuffer sb = new StringBuffer(string.length());
	    // true if last char was blank
	    boolean lastWasBlankChar = false;
	    int len = string.length();
	    char c;

	    for (int i = 0; i < len; i++)
	        {
	        c = string.charAt(i);
	        if (c == ' ') {
	            // blank gets extra work,
	            // this solves the problem you get if you replace all
	            // blanks with &nbsp;, if you do that you loss 
	            // word breaking
	            if (lastWasBlankChar) {
	                lastWasBlankChar = false;
	                sb.append("&nbsp;");
	                }
	            else {
	                lastWasBlankChar = true;
	                sb.append(' ');
	                }
	            }
	        else {
	            lastWasBlankChar = false;
	            //
	            // HTML Special Chars
	            if (c == '"')
	                sb.append("&quot;");
	            else if (c == '&')
	                sb.append("&amp;");
	            //else if (c == '<')
	            //    sb.append("&lt;");
	            //else if (c == '>')
	            //    sb.append("&gt;");
	            else if (c == '\n')
	                // Handle Newline
	            	sb.append("<br/>");
	                //sb.append("&lt;br/&gt;");
	            else {
	                int ci = 0xffff & c;
	                if (ci < 160 )
	                    // nothing special only 7 Bit
	                    sb.append(c);
	                else {
	                    // Not 7 Bit use the unicode system
	                    sb.append("&#");
	                    sb.append(new Integer(ci).toString());
	                    sb.append(';');
	                    }
	                }
	            }
	        }
	    return sb.toString();
	}
	
	private static HashMap<String,String> htmlEntities;
	  static {
	    htmlEntities = new HashMap<String,String>();
	    htmlEntities.put("&lt;","<")    ; htmlEntities.put("&gt;",">");
	    htmlEntities.put("&amp;","&")   ; htmlEntities.put("&quot;","\"");
	    htmlEntities.put("&agrave;","�"); htmlEntities.put("&Agrave;","�");
	    htmlEntities.put("&acirc;","�") ; htmlEntities.put("&auml;","�");
	    htmlEntities.put("&Auml;","�")  ; htmlEntities.put("&Acirc;","�");
	    htmlEntities.put("&aring;","�") ; htmlEntities.put("&Aring;","�");
	    htmlEntities.put("&aelig;","�") ; htmlEntities.put("&AElig;","�" );
	    htmlEntities.put("&ccedil;","�"); htmlEntities.put("&Ccedil;","�");
	    htmlEntities.put("&eacute;","�"); htmlEntities.put("&Eacute;","�" );
	    htmlEntities.put("&egrave;","�"); htmlEntities.put("&Egrave;","�");
	    htmlEntities.put("&ecirc;","�") ; htmlEntities.put("&Ecirc;","�");
	    htmlEntities.put("&euml;","�")  ; htmlEntities.put("&Euml;","�");
	    htmlEntities.put("&iuml;","�")  ; htmlEntities.put("&Iuml;","�");
	    htmlEntities.put("&ocirc;","�") ; htmlEntities.put("&Ocirc;","�");
	    htmlEntities.put("&ouml;","�")  ; htmlEntities.put("&Ouml;","�");
	    htmlEntities.put("&oslash;","�") ; htmlEntities.put("&Oslash;","�");
	    htmlEntities.put("&szlig;","�") ; htmlEntities.put("&ugrave;","�");
	    htmlEntities.put("&Ugrave;","�"); htmlEntities.put("&ucirc;","�");
	    htmlEntities.put("&Ucirc;","�") ; htmlEntities.put("&uuml;","�");
	    htmlEntities.put("&Uuml;","�")  ; htmlEntities.put("&nbsp;"," ");
	    htmlEntities.put("&copy;","\u00a9");
	    htmlEntities.put("&reg;","\u00ae");
	    htmlEntities.put("&euro;","\u20a0");
	  }

	  public static final String unescapeHTML(String source, int start){
	     int i,j;

	     i = source.indexOf("&", start);
	     if (i > -1) {
	        j = source.indexOf(";" ,i);
	        if (j > i) {
	           String entityToLookFor = source.substring(i , j + 1);
	           String value = (String)htmlEntities.get(entityToLookFor);
	           if (value != null) {
	             source = new StringBuffer().append(source.substring(0 , i))
	                                   .append(value)
	                                   .append(source.substring(j + 1))
	                                   .toString();
	             return unescapeHTML(source, i + 1); // recursive call
	           }
	         }
	     }
	     return source;
	  }

	/* TODO: Create org.devtcg.provider.dao.* classes for this. */
	private class ChannelPost
	{
		public String title;
		public Date date;
		public String desc = new String();
		public String link;
		public String author;

		public ChannelPost()
		{
			/* Empty. */
			desc = new String();
		}

		public void setDate(String str)
		{
			date = DateUtils.parseDate(str);

			if (date == null)
				date = new Date();
		}

		public String getDate()
		{
			return DateUtils.formatDate(mPostBuf.date);
		}
	}
}
