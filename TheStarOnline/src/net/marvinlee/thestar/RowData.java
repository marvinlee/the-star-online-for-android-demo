package net.marvinlee.thestar;


/**
 * Data type used for custom adapter. Single item of the adapter.
 */
public class RowData {
	protected String mTitle;
	protected String mUrl;
	protected String mDescription;
	protected String mDate;

	public RowData(String title, String url, String description, String date) {
		mTitle = title;
		mUrl = url;
		mDescription = description;
		mDate = date;
	}

	@Override
	public String toString() {
		return mTitle + " [" + mUrl + "]"+ " " + mDescription;
	}
}
