package net.marvinlee.thestar;
 
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter; 
import android.widget.ListView;

public class ChannelListActivity extends ListActivity{
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	  super.onCreate(savedInstanceState);
    	   
    	  setListAdapter(new ArrayAdapter<String>(this,
    	          android.R.layout.simple_list_item_1, CHANNELS));
    	  getListView().setTextFilterEnabled(true);

    }
    
    public void onListItemClick(ListView parent, View v, int position, long id) {
		Log.i("TheStar", "onListItemClick called");
		ArrayAdapter adapter = (ArrayAdapter) parent.getAdapter();
		Log.i("TheStar", "Getting item at position:" + position);
		String row = (String)adapter.getItem(position);
		Log.i("TheStar", "Setting other props");
		/*
		Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Selected");
		builder.setMessage(position + " -> " + row);
		builder.setPositiveButton("ok", null);
		builder.show();
 */
		
		Intent feedIntent = new Intent(this.getApplicationContext(), ChannelTabFeedActivity.class);
		feedIntent.putExtra("feedTitle", CHANNELS[position]);
		feedIntent.putExtra("feedUrl", CHANNELS_FEED[position]);
		startActivity(feedIntent);
		Log.i("TheStar", "Setting other props");
	}

    static final String[] CHANNELS = new String[] {
    	"Nation", 
    	"Business", 
    	"Sports", 
    	"Technology", 
    	"World Updates",
    	"Most Viewed Nation", 
    	"Most Viewed Business", 
    	"Most Viewed Sports",
    	"Most Viewed Entertainment",
    	"Metro - Central",
    	"Metro - North",
    	"Metro - South & East",
    	"Columnists",
    	"Opinion", 
    	"Bookshelf", 
    	"Parenting", 
    	"Living", 
    	"Focus", 
    	"Health", 
    	"Arts & Fashion",
    	"Travel & Adventure"
      };

    static final String[] CHANNELS_FEED = new String[] {
    	"http://thestar.com.my/rss/nation.xml", 
    	"http://thestar.com.my/rss/business.xml", 
    	"http://thestar.com.my/rss/sports.xml", 
    	"http://thestar.com.my/rss/technology.xml", 
    	"http://thestar.com.my/rss/worldupdates.xml",
    	"http://thestar.com.my/rss/mostview/nation.xml", 
    	"http://thestar.com.my/rss/mostview/business.xml", 
    	"http://thestar.com.my/rss/mostview/sports.xml",
    	"http://thestar.com.my/rss/mostview/entertainment.xml",
    	"http://thestar.com.my/rss/central.xml",
    	"http://thestar.com.my/rss/north.xml",
    	"http://thestar.com.my/rss/southneast.xml",
    	"http://thestar.com.my/rss/columnists.xml",
    	"http://thestar.com.my/rss/opinion.xml", 
    	"http://thestar.com.my/rss/lifebookshelf.xml", 
    	"http://thestar.com.my/rss/lifeparenting.xml", 
    	"http://thestar.com.my/rss/lifeliving.xml", 
    	"http://thestar.com.my/rss/lifefocus.xml", 
    	"http://thestar.com.my/rss/health.xml", 
    	"http://thestar.com.my/rss/lifearts.xml",
    	"http://thestar.com.my/rss/lifetravel.xml"
      };
     
}
