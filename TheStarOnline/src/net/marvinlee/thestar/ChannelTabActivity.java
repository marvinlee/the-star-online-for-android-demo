package net.marvinlee.thestar;
   

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.TabActivity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.util.Linkify;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;

public class ChannelTabActivity extends TabActivity {
    /** Called when the activity is first created. */
    private SharedPreferences mPrefs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_admob);
        
        //Log.i("TheStar", "Start");
        
        //Try get data
		Bundle dataBundle = (savedInstanceState == null)
		? getIntent().getExtras()
		: savedInstanceState;
		String selectedFeedTitle = "Feeds";

        
        /*
		if (dataBundle != null)
		{

			//selectedFeedTitle = dataBundle.getString("feedTitle"); 
			if ((selectedFeedTitle == null) || ("".equals(selectedFeedTitle)))
			{
				selectedFeedTitle = "Feeds";
			}
		}*/
        
        //Test get phone number
        TelephonyManager phoneManager = (TelephonyManager)this.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        //Log.i("TheStar", "Phone number:"+phoneManager.getDeviceId());
        //Log.i("TheStar", "Imei  number:"+phoneManager.getLine1Number());
        //Log.i("TheStar", "SIM serial  :"+phoneManager.getSimSerialNumber());
        
        
        TabHost mTabHost = getTabHost();
        
        mTabHost.addTab(mTabHost.newTabSpec("tab_channel").setIndicator("", getResources().getDrawable(R.drawable.logo)).setContent(new Intent(this, ChannelListActivity.class))); 
        mTabHost.addTab(mTabHost.newTabSpec("tab_feed").setIndicator(selectedFeedTitle).setContent(R.id.textview2));
      
        
        int currentTab = 0;
        /*
        if (getIntent().getExtras() != null)
        {
        	currentTab = Integer.parseInt("" + getIntent().getExtras().get("position"));
        }*/
        mTabHost.setCurrentTab(currentTab);

        //Log.i("TheStar", "Done tabs");

		//Intent intent = new Intent(this.getApplicationContext(), ChannelListActivity.class);
		//startActivity(intent);
  	    
    }
    /* Creates the menu items */
    public boolean onCreateOptionsMenu(Menu menu) {
  
        menu.add(0, 1, 1, "About"); 
 
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

    	//Log.d("TheStar", "Menu selected id:"+ item.getItemId());

        switch (item.getItemId()) {
        case 1:

        	//Log.d("TheStar", "Creating about"); 
    		String title1 = "About The Star Online"; 
    		String text1 = "\nThe Star Online is an information portal on the Internet. \nIt was launched on June 23, 1995 as the Internet edition of Malaysia's leading English-language newspaper, The Star, but has since evolved - with added features and functions - to move away from merely being an online newspaper.\n";
    		text1 += "\n\nThe Star Online is the property of Star Publications (Malaysia) Berhad (co. no. 10894-D), a company incorporated in Malaysia. ";
    		String title2 = "\n\nAbout The Star Online for Android";
    		String text2 = "";//"\nThe Star Online for Android is developed and maintained by Marvin Lee and is not affiliated with Star Publications in anyway.";
    		text2 += "\n\nThis Android app is developed solely for the benefit of The Star readers and Malaysians in general.";
    		PackageInfo pInfo = null;
				try {
					pInfo = getPackageManager().getPackageInfo("net.marvinlee.thestar", 0);
				} catch (NameNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (pInfo != null)
			{
				text2 += "\n\nVersion:" + pInfo.versionName;
			}
			text2 += "\n\nFor more info, contact Marvin Lee at :";
    		
    		Builder builder;
    		AlertDialog alertDialog;

			//Context mContext = getApplicationContext(); 
			LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.about_view, //null);
			                               (ViewGroup) findViewById(R.id.layout_root));

			TextView textTitle1 = (TextView) layout.findViewById(R.id.aboutview_title1);
			textTitle1.setText(title1);
			TextView textContent1 = (TextView) layout.findViewById(R.id.aboutview_text1);
			textContent1.setText(text1);
			
			TextView textTitle2 = (TextView) layout.findViewById(R.id.aboutview_title2);
			textTitle2.setText(title2);
			TextView textContent2 = (TextView) layout.findViewById(R.id.aboutview_text2);
			textContent2.setText(text2);
			textContent2.setAutoLinkMask(Linkify.ALL);
			 
    		builder = new AlertDialog.Builder(this);
			builder.setTitle("The Star Online for Android");
			builder.setView(layout);
			builder.setPositiveButton("OK", null);
			builder.show();
			 
            return true; 
        }
        return false;
 
    } 
}